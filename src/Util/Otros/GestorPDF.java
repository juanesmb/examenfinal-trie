package Util.Otros;

import com.itextpdf.kernel.colors.Color;
import com.itextpdf.kernel.colors.DeviceRgb;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfPage;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.VerticalAlignment;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import ufps.util.colecciones_seed.Trie;
import ufps.util.colecciones_seed.TrieNode;

public class GestorPDF {

    private final Trie trie;
    private PdfPage page;
    private final Color GREEN = new DeviceRgb(9, 124, 9);
    private final Color LIGHT_GREEN = new DeviceRgb(196, 255, 196);
    private final Color YELLOW = new DeviceRgb(255, 242, 0);
    private final Color BLACK = new DeviceRgb(0, 0, 0);
    private final int RADIO = 15;
    private final String DEST = "arbolTrie.pdf";

    public GestorPDF(Trie trie) {
        this.trie = trie;
    }

    public void generarPDF(String word) throws Exception {
        PdfWriter writer = new PdfWriter(DEST);
        PdfDocument pdfDoc = new PdfDocument(writer);

        try ( Document doc = new Document(pdfDoc)) {
            this.page = pdfDoc.addNewPage();

            Rectangle size = this.page.getPageSize();
            float x = size.getWidth() / 2;
            float y = size.getHeight() - doc.getTopMargin() - 100;

            if (word != null) {
                escribirTitulo(doc, word);
            }

            Punto punto = new Punto(x, y);
            insertarNodo(doc, punto, size.getWidth(), trie.getRaiz());
            File file = new File(DEST);
            Desktop.getDesktop().open(file);
        } catch (Exception e) {
            throw e;
        }
    }

    public void generarPDF() throws Exception {
        generarPDF(null);
    }

    public void insertarNodo(Document doc, Punto punto, float off, TrieNode nodo) throws IOException {
        pintarCirculo(punto, nodo);
        escribir(doc, punto, nodo.getDato());

        int cantidad = nodo.getCantidad();

        if (cantidad == 0) {
            return;
        }

        TrieNode[] hijos = nodo.getHijos();
        double equis;

        double x = punto.getX();
        double y = punto.getY();

        float espacio = off / cantidad;
        float posicion = espacio / 2;

        for (int j = 0; j < cantidad; j++) {
            equis = x;

            if (estaPorIzquierda(cantidad, j + 1)) {
                equis = x - posicion - (distancia(cantidad, j + 1) * espacio);
            }

            if (estaPorDerecha(cantidad, j + 1)) {
                equis = x + posicion + (distancia(cantidad, j + 1) * espacio);
            }

            Punto p = new Punto(equis, y - 50);

            pintarLinea(p, punto);
            insertarNodo(doc, p, espacio, hijos[j]);
        }
    }

    private boolean estaPorIzquierda(int length, int j) {
        if (length % 2 == 0) {
            return j <= length / 2;
        }

        return j < (length / 2) + 1;
    }

    private boolean estaPorDerecha(int length, int j) {
        if (length % 2 == 0) {
            return j > length / 2;
        }

        return j > ((length / 2) + 1);
    }

    private int distancia(int length, int i) {
        int distancia;
        int pivote = (length / 2) + 1;

        if (length % 2 == 0) {
            boolean derecha = i > length / 2;
            distancia = derecha ? i - pivote : i - (length / 2);
        } else {
            distancia = i - pivote;
        }

        return Math.abs(distancia);
    }

    private void pintarLinea(Punto a, Punto b) {
        PdfCanvas canvas = new PdfCanvas(this.page);

        canvas.moveTo(b.getX(), b.getY() - RADIO);
        canvas.lineTo(a.getX(), a.getY() + RADIO);
        canvas.setStrokeColor(GREEN);
        canvas.closePathStroke();
    }

    private void pintarCirculo(Punto punto, TrieNode nodo) {
        double x = punto.getX();
        double y = punto.getY();

        PdfCanvas circle = new PdfCanvas(this.page).circle(x, y, RADIO);
        circle.setStrokeColor(GREEN);

        if (nodo.esBuscado()) {
            circle.setFillColor(YELLOW);
            circle.closePathFillStroke();
            nodo.setBuscado(false);
        } else if (nodo.esFinal()) {
            circle.setFillColor(LIGHT_GREEN);
            circle.closePathFillStroke();
        }

        circle.closePathStroke();
    }

    private void escribirTitulo(Document document, String texto) {
        Paragraph p = new Paragraph("La palabra buscada es: " + texto);
        p.setTextAlignment(TextAlignment.CENTER);
        document.add(p);
    }

    private void escribir(Document document, Punto punto, String dato) {
        Paragraph p = new Paragraph(dato);

        p.setWidth(RADIO * 2);
        p.setHeight(RADIO * 2);
        p.setFontColor(BLACK);
        p.setFixedPosition((float) punto.getX() - RADIO, (float) punto.getY() - RADIO, RADIO * 2);
        p.setTextAlignment(TextAlignment.CENTER);
        p.setVerticalAlignment(VerticalAlignment.MIDDLE);

        document.add(p);
    }
}
