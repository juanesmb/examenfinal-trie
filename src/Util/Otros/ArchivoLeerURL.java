/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Util.Otros;

import java.net.*;
import java.io.*;

/**
 * Clase que permita leer un archivo de texto plano desde una URL
 *
 * @author Marco Adarme
 */
public class ArchivoLeerURL {

    //Almacena la dirección URL del archivo
    private String direccion;

    /**
     * Permite crear un objeto de tipo ArchivoLeerURL especificando su URL
     *
     * @param direccion la URL donde está el archivo
     */
    public ArchivoLeerURL(String direccion) {
        this.direccion = direccion;
    }

    public ArchivoLeerURL() {
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * Método que retorna el archivo en un vector de Object. Cada línea se
     * almacena secuencialmente en cada posición del vector
     *
     * @return un vector con el archivo almacenado en la URL
     * @throws MalformedURLException
     */
    public Object[] leerArchivo() throws MalformedURLException {
        java.util.ArrayList<String> l = new java.util.ArrayList();

        try {
            // Indicamos la URL donde nos conectamos
            URL url = new URL(this.getDireccion());
            // Buffer con los datos recibidos
            BufferedReader in = null;

            try {
                // Volcamos lo recibido al buffer
                in = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"));
            } catch (Throwable t) {
                System.out.println(t.getMessage());
            }

            // Transformamos el contenido del buffer a texto
            String inputLine;

            // Mientras haya cosas en el buffer las volcamos a las
            // cadenas de texto 
            while ((inputLine = in.readLine()) != null) {
                l.add(inputLine);
            }

            in.close();

        } catch (MalformedURLException ex1) {
            System.out.println("URL erronea: " + ex1.getMessage());
            throw ex1;
        } catch (IOException ex2) {
            System.out.println("Error IO:" + ex2.getMessage());
        } catch (Exception e) {
            throw e;
        }

        return (l.toArray());

    }
}
