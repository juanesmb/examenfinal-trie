package ufps.util.colecciones_seed;

/**
 *
 * @author 1151941
 * @author 1151858
 */

public class TrieNode {
    private final int ALFABETO = 32;
    
    private char caracter;
    private String digrafo;

    private TrieNode raiz;
    private final TrieNode[] hijos;
    private int cantidad; 
    
    private boolean esFinal;
    private boolean esDigrafo;
    private boolean buscado;

    public TrieNode() {
        this.hijos = new TrieNode[ALFABETO];
        this.esFinal = false;
        this.cantidad = 0;
        this.buscado = false;
    }

    public TrieNode(char c, TrieNode raiz) {
        this();
        this.raiz = raiz;
        this.caracter = c;
        this.esDigrafo = false;
    }

    public TrieNode(String digrafo, TrieNode raiz) {
        this();
        this.raiz = raiz;
        this.digrafo = digrafo;
        this.esDigrafo = true;
    }
    
    public boolean esta(char c) {
        return hijos[index(c)] != null;
    }

    public boolean esta(String digrafo) {
       return hijos[index(digrafo)] != null;
    }

    /**
     * Obtener el caracter asociado al nodo.
     * @param c
     * @return 
     */
    public TrieNode get(char c) {
        return hijos[index(c)];
    }
    
    /**
     * Obtener el dígrafo asociado al nodo, en caso de que exista.
     * @param digrafo
     * @return Dígrafo asociado al nodo
     */
    public TrieNode get(String digrafo){
        return hijos[index(digrafo)];
    }

    /**
     * Insertar un nodo asociado a un caracter
     * @param c Caracter asociado al nodo.
     * @return nodo creado.
     */
    public TrieNode insertar(char c) {
        TrieNode hijo = new TrieNode(c, this);
        this.cantidad++;
        this.hijos[index(c)] = hijo;
        return hijo;
    }
    
    /**
     * Insertar un nodo asociado a un dígrafo.
     * @param digrafo Dígrafo asociado al nodo.
     * @return Nodo creado.
     */
    public TrieNode insertar(String digrafo){
        TrieNode hijo = new TrieNode(digrafo, this);
        this.cantidad++;
        this.hijos[index(digrafo)] = hijo;
        return hijo;
    }
    
    /* Posiciones asociadas a cada caracter y/o dígrafo.
     [0],[1],[2],[3], [4],[5],[6],[7],[8], [9],[10],[11],[12],[13],[14],[15]
     [a],[b],[c],[ch],[d],[e],[f],[g],[gu],[h],[i], [j], [k], [l], [ll] [m]
    
     [16],[17],[18],[19],[20],[21],[22],[23],[24],[25],[26],[27],[28],[29],[30],[31]
     [n], [ñ], [o], [p], [q], [qu],[r], [rr], [s], [t],[u], [v], [w], [x], [y], [z]
    */
    
    /**
     * Buscar la posición de un nodo hijo asociado a un caracter. 
     * @param c caracter asociado al nodo a buscar.
     * @return Posición del nodo en el arreglo de hijos.
     */
    private int index(char c){
        if(c <= 'c'){
            return c - 97;
        }
        
        if(c <= 'g'){
           return c - 96;
        }

        if(c <= 'l'){
            return c - 95;
        }
        
        if(c <= 'n') {
            return c - 94 ;
        }
        
        if (c == 'ñ') {
            return 17;
        }
        
        if(c <= 'q'){
            return c - 93;
        }
        
        if (c == 'r'){
            return 22;
        }
        
        return c - 91;
    }
    
    /**
     * Buscar la posición de un nodo hijo asociado a un dígrafo.
     * @param digrafo Dígrafo asociado al nodo a buscar.
     * @return Posición del nodo en el arreglo de hijos.
     */
    private int index(String digrafo){
         switch (digrafo) {
            case "ch":
                return 3;
            case "gu":
                return 8;
            case "ll":
                return 14;
            case "qu":
                return 21;
            case "rr":
                return 23;
            default:
                return -1;
        }   
    }

    public String getDigrafo() {
        return digrafo;
    }

    /**
     * Devuelva los hijos existentes
     * @return arreglo con los hijos existentes.
     */
    public TrieNode[] getHijos() {
        TrieNode[] hijos = new TrieNode[this.cantidad];
        int i = 0;
        
        for(TrieNode hijo : this.hijos){
            if(hijo != null){
                hijos[i] = hijo;
                i++;
            }
        }
        
        return hijos;
    }
   
    public char getCaracter() {
        return this.caracter;
    }
    
    public void setEsFinal(boolean valor) {
        this.esFinal = valor;
    }

    public boolean esFinal() {
        return this.esFinal;
    }
    
    /**
     * Obtener cantidad de hijos en el nodo.
     * @return Cantidad de hijos del nodo.
     */
    public int getCantidad(){
        return this.cantidad;
    }

    public boolean esDigrafo()
    {
        return this.esDigrafo;
    }
    
    public String getDato()
    {
        if(this.esDigrafo)
            return this.digrafo;
        
        return Character.toString(caracter);
    }
    
    public TrieNode getRaiz(){
        return this.raiz;
    }

    public boolean esBuscado() {
        return buscado;
    }

    public void setBuscado(boolean buscado) {
        this.buscado = buscado;
    }
}
