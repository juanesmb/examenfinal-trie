package ufps.util.colecciones_seed;

import Util.Otros.ArchivoLeerURL;
import java.net.MalformedURLException;

/**
 *
 * @author 1151941
 * @author 1151858
 */
public class Trie {

    private final TrieNode raiz;

    public Trie() {
        this.raiz = new TrieNode();
    }

    public void cargar(String url) throws MalformedURLException {
        ArchivoLeerURL file = new ArchivoLeerURL(url);
        Object v[] = file.leerArchivo();

        for (Object filas : v) {
            String datos = filas.toString();
            String[] words = datos.split(", ");

            for (String word : words) {
                this.insertar(word);
            }
        }
    }

    public void insertar(String word) {
        insertar(word, 0, this.raiz);
    }

    private void insertar(String word, int i, TrieNode nodo) {
        if (i == word.length()) {
            nodo.setEsFinal(true);
            return;
        }

        char caracter = word.charAt(i);
        char next = i < (word.length() - 1) ? word.charAt(i + 1) : 0;

        if (!Character.isLetter(caracter)) {
            throw new RuntimeException("No es una palabra válida");
        }

        TrieNode siguiente = nodo.get(caracter);
        int indice = i + 1;

        if (esDigrafo(caracter, next)) {
            String digrafo = crearDigrafo(caracter, next);

            if (!nodo.esta(digrafo)) {
                nodo.insertar(digrafo);
            }

            siguiente = nodo.get(digrafo);
            indice++;
        } else if (!nodo.esta(caracter)) {
            siguiente = nodo.insertar(caracter);
        }

        insertar(word, indice, siguiente);
    }

    public boolean buscar(String word) {
        return buscar(word, 0, this.raiz);
    }

    private boolean buscar(String word, int i, TrieNode nodo) {
        if (i == word.length()) {
            return true;
        }

        char caracter = word.charAt(i);
        char next = i < (word.length() - 1) ? word.charAt(i + 1) : 0;

        if (!Character.isLetter(caracter)) {
            throw new RuntimeException("No es una palabra válida");
        }

        TrieNode siguiente = nodo.get(caracter);
        int indice = i + 1;

        if (esDigrafo(caracter, next)) {
            String digrafo = crearDigrafo(caracter, next);

            if (!nodo.esta(digrafo)) {
                return false;
            }

            siguiente = nodo.get(digrafo);
            indice++;
        } else if (!nodo.esta(caracter)) {
            return false;
        }

        boolean esta = buscar(word, indice, siguiente);

        if (esta) {
            siguiente.setBuscado(true);
        }

        return esta;
    }

    /**
     * Validar si un caracter está asociado a un dígrafo
     *
     * @param caracter Primer caracter a evaluar
     * @param siguiente Caracter contíguo.
     * @return booleano
     */
    private boolean esDigrafo(char caracter, char siguiente) {
        return (caracter == 'c' && siguiente == 'h')
                || (caracter == 'g' && siguiente == 'u')
                || (caracter == 'l' && siguiente == 'l')
                || (caracter == 'q' && siguiente == 'u')
                || (caracter == 'r' && siguiente == 'r');
    }

    private String crearDigrafo(char caracter, char next) {
        char[] par = new char[]{caracter, next};
        return new String(par);
    }

    /**
     * Obtener la altura o suma de niveles que tiene el Trie.
     *
     * @return Altura.
     */
    public int getAltura() {
        return getAltura(this.raiz);
    }

    /**
     * Obtener la altura en un nodo específico.
     *
     * @param nodo Nodo a calcular la altura.
     * @return Altura.
     */
    private int getAltura(TrieNode nodo) {
        if (nodo.esFinal()) {
            return 1;
        }

        int altura = 1;

        for (TrieNode hijo : nodo.getHijos()) {
            int alturaHijo = getAltura(hijo);

            if (alturaHijo > altura) {
                altura = alturaHijo;
            }
        }

        return (altura + 1);

    }

    /**
     * Devuelve la máxima anchura del Trie.
     *
     * @return Máxima anchura
     */
    public int getAnchura() {
        return getAnchura(raiz);
    }

    /**
     * Devuelve la máxima anchura a partir de un nodo.
     *
     * @param raiz Nodo a partir del cual se obtiene la máxima anchura.
     * @return Máxima anchura a partir de un nodo.
     */
    private int getAnchura(TrieNode raiz) {
        if (raiz.esFinal()) {
            return 1;
        }

        int max = 0;
        Cola<TrieNode> cola = new Cola();
        cola.enColar(raiz);

        while (!cola.esVacia()) {
            int size = cola.getTamanio();
            max = Math.max(max, size);

            while (size > 0) {
                TrieNode nodo = cola.deColar();

                for (TrieNode hijo : nodo.getHijos()) {
                    cola.enColar(hijo);
                }

                size--;
            }
        }

        return max;
    }

    public TrieNode getRaiz() {
        return raiz;
    }

}
