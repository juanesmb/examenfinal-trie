/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Util.Otros.GestorPDF;
import ufps.util.colecciones_seed.Trie;

/**
 *
 * @author 1151941 - 1151858
 */
public class Consola {

    public static void main(String[] args) {
        String link = "https://gitlab.com/-/snippets/2046364/raw/master/parabras-trie.txt";

        try {
            Trie trie = new Trie();
            trie.cargar(link);
            trie.buscar("mar");
            
            GestorPDF gestor = new GestorPDF(trie);
            gestor.generarPDF("mar");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
